package main

import (
	"errors"
	"math/rand"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func generateError() error {

	if rand.Float32() < 0.15 {
		return errors.New("Generated error")
	}

	return nil
}
