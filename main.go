package main

import (
	"fmt"
	"golang.org/x/sync/errgroup"
	"log"
	"sync"
)

const chunkSize = 3
const numOfWorkers = 3

type inputChunk []int
type outputChunk []int

func main() {

	//  Это некоторые данные из базы
	slc := make([]int, 11)

	for i := range slc {
		slc[i] = i
	}

	// Назрезаем данные из базы на чанки и отправляем их в канал input
	input := producer(slc)

	// создаём результирующий канал, в который будут писаться данные из воркеров
	output := make(chan outputChunk)

	// Инструмент ожидания завершения работы всех воркеров
	wg := &sync.WaitGroup{}

	// группа горутин, в которых могут произойти ошибки - воркеры и консумер
	var eg errgroup.Group

	// Запускаем функцию, обрабатывающую результирующий канал
	eg.Go(func() error {
		return consumer(output)
	})

	// Запускаем несколько воркеров. Они читают из input'а и результат передают в output канал
	for i := 0; i < numOfWorkers; i++ {
		wg.Add(1)
		eg.Go(func() error {
			return worker(i, wg, input, output)
		})
	}

	// Ждём завершения работы всех воркеров, чтобы закрыть результирующий канал
	go func(wg *sync.WaitGroup, output chan outputChunk) {
		wg.Wait()
		close(output)
	}(wg, output)

	// Ждём завершения работы всех корутин эрроргруппы или ошибку из них
	if err := eg.Wait(); err != nil {
		log.Fatalf("Encountered error: %v", err)
	}

	log.Println("Successfully finished.")
}

// producer нарезает входные данные на чанки и отправляет чанки в канал.
// Закрывает канал, когда все данные переданы в него.
func producer(input []int) <-chan inputChunk {

	chunksCh := make(chan inputChunk)

	go func() {
		inputLen := len(input)
		rem := inputLen % chunkSize
		for i := 0; i+chunkSize <= inputLen; i += chunkSize {
			chunksCh <- inputChunk(input[i : i+chunkSize])
		}
		chunksCh <- inputChunk(input[inputLen-rem:])
		close(chunksCh)
	}()

	return chunksCh
}

// worker считывает чанки из входящего канала,
func worker(workerNum int, wg *sync.WaitGroup, chunksCh <-chan inputChunk, output chan outputChunk) error {

	defer wg.Done()

	for chunk := range chunksCh {
		log.Printf("worker: %d, chunk: %v\n", workerNum, chunk)
		// Здесь, получив чанк, обрабатываем его (идём в базу, матчим, фильтруем, проверяем итд)

		// В этом процессе может произойти ошибка:
		err := generateError()
		if err != nil {
			return fmt.Errorf("Error in the worker number %d: %v", workerNum, err)
		}

		// Если выходной канал открыт (консумер не покрекшился из-за ошибки)
		// создаём результирующий чанк на основе результатов
		// и отправляем в результирующий канал
		select {
		case output <- outputChunk(chunk):
		default:
			log.Println("output is closed, can't send")
			return nil
		}

	}
	log.Println("worker is closed")

	return nil
}

// consumer считывает данные из результирующего канала и
func consumer(output <-chan outputChunk) error {

	for chunk := range output {
		log.Printf("output: %v\n", chunk)
		// здесь сохраняем чанк в бд
		// (или накопляем сначала несколько чанков перед сохранением в бд)

		// В этом процессе может произойти ошибка:
		err := generateError()
		if err != nil {
			return fmt.Errorf("Error in the consumer: %v", err)
		}

	}
	log.Println("consumer is closed")

	return nil
}
